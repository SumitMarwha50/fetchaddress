package com.example.fetchaddress.utils

import com.example.fetchaddress.core.data.ErrorEntity
import com.example.fetchaddress.core.data.ResultState
import com.example.fetchaddress.feature.search.dto.FetchAddressResponse

object AddressListGenerator {

    fun createMockNewsData(data: FetchAddressResponse?): ResultState<FetchAddressResponse> {
        return data?.let {
            ResultState.Success(data)
        } ?: ResultState.Error(ErrorEntity.Error("0", "Error"))
    }
}