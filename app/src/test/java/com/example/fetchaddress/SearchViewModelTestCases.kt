package com.example.fetchaddress

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.fetchaddress.core.data.ResultState
import com.example.fetchaddress.feature.search.dto.Address
import com.example.fetchaddress.feature.search.dto.FetchAddressResponse
import com.example.fetchaddress.feature.search.repository.FetchAddressService
import com.example.fetchaddress.feature.search.repository.SearchRepository
import com.example.fetchaddress.feature.search.viewmodel.SearchViewModel
import com.example.fetchaddress.utils.AddressListGenerator
import com.example.newsfeed.utils.mock
import junit.framework.Assert
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.hamcrest.core.IsNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito

@RunWith(JUnit4::class)
class SearchViewModelTestCases {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private val fetchAddressService = Mockito.mock(FetchAddressService::class.java)
    private lateinit var searchRepository: SearchRepository
    private lateinit var searchViewModel: SearchViewModel

    @Before
    fun doBefore() {
        searchRepository = SearchRepository(fetchAddressService)
        searchViewModel = SearchViewModel(searchRepository)
    }

    @Test
    fun testNotNull() {
        MatcherAssert.assertThat(fetchAddressService, CoreMatchers.notNullValue())
        MatcherAssert.assertThat(searchRepository, CoreMatchers.notNullValue())
        MatcherAssert.assertThat(searchViewModel, CoreMatchers.notNullValue())
    }

    @Test
    fun testAddressResponseResult() {
        val observer = mock<Observer<ResultState<FetchAddressResponse>>>()
        val addressList = ArrayList<Address>()
        addressList.add(Address())
        val mockedData = AddressListGenerator.createMockNewsData(FetchAddressResponse(addressList = addressList))
        when (mockedData) {
            is ResultState.Success -> {
                MatcherAssert.assertThat(mockedData.data, IsNull.notNullValue())
            }
        }
        searchViewModel.liveAddressList.observeForever(observer)
        searchViewModel.mutableAddressList.value = mockedData
        Mockito.verify(observer).onChanged(mockedData)
        Assert.assertEquals(searchViewModel.addressList.size, 1)
    }

    @Test
    fun testAddressResponseNoResult() {
        val observer = mock<Observer<ResultState<FetchAddressResponse>>>()
        val mockedData = AddressListGenerator.createMockNewsData(null)
        when (mockedData) {
            is ResultState.Error -> {
                MatcherAssert.assertThat(mockedData.error.errorMessage, IsNull.notNullValue())
            }
        }
        searchViewModel.liveAddressList.observeForever(observer)
        searchViewModel.mutableAddressList.value = mockedData
        Mockito.verify(observer).onChanged(mockedData)
        Assert.assertEquals(searchViewModel.addressList.size, 0)
    }

}