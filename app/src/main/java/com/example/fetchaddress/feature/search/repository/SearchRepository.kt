package com.example.fetchaddress.feature.search.repository

import androidx.lifecycle.MutableLiveData
import com.example.fetchaddress.core.data.ResultState
import com.example.fetchaddress.core.network.ResponseListenerHandling
import com.example.fetchaddress.core.utils.Urls
import com.example.fetchaddress.feature.search.dto.FetchAddressResponse
import javax.inject.Inject

class SearchRepository @Inject constructor(
    private val fetchAddressService: FetchAddressService
) {

    fun getAddress(mutableAddress: MutableLiveData<ResultState<FetchAddressResponse>>,
                   query: String, city: String) {
        val url = Urls.addressList
        mutableAddress.postValue(ResultState.Loading(true))
        fetchAddressService.getAddress(url, query, city)
            .enqueue(ResponseListenerHandling(mutableAddress))
    }

}