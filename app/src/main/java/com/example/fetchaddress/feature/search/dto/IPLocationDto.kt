package com.example.fetchaddress.feature.search.dto

import com.google.gson.annotations.SerializedName

data class IPLocationDto(
    @SerializedName("ip")
    val ip: String? = null,

    @SerializedName("country_code")
    val countryCode: String? = null,

    @SerializedName("country_name")
    val countryName: String? = null,

    @SerializedName("region_code")
    val regionCode: String? = null,

    @SerializedName("region_name")
    val regionName: String? = null,

    @SerializedName("city")
    val city: String? = null,

    @SerializedName("zip_code")
    val zipCode: String? = null,

    @SerializedName("time_zone")
    val timeZone: String? = null,

    @SerializedName("latitude")
    val latitude: Double? = null,

    @SerializedName("longitude")
    val longitude: Double? = null,

    @SerializedName("metro_code")
    val metroCode: Int? = null
)