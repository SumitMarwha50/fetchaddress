package com.example.fetchaddress.feature.search.dto

import com.google.gson.annotations.SerializedName

data class FetchAddressResponse (
    @SerializedName("autoCompleteRequestString")
    val autoCompleteRequestString: String? = null,
    @SerializedName("addressList")
    val addressList: List<Address>? = null
)

data class Address (
    @SerializedName("id")
    val id: String? = null,
    @SerializedName("pinCode")
    val pinCode: String? = null,
    @SerializedName("city")
    val city: String? = null,
    @SerializedName("cityBoundaryBreached")
    val cityBoundaryBreached: String? = null,
    @SerializedName("pinCodeBoundaryBreached")
    val pinCodeBoundaryBreached: String? = null,
    @SerializedName("addressType")
    val addressType: String? = null,
    @SerializedName("addressString")
    val addressString: String? = null,
    @SerializedName("latitude")
    val latitude: String? = null,
    @SerializedName("longitude")
    val longitude: String? = null,
    @SerializedName("errorMargin")
    val errorMargin: String? = null
)