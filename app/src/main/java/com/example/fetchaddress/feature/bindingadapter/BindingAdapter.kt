package com.example.fetchaddress.feature.bindingadapter

import android.view.View
import androidx.databinding.BindingAdapter

@BindingAdapter("app:visibility")
fun visibility(view: View, visibility: Boolean) {
    if (visibility) {
        view.visibility = View.VISIBLE
    } else {
        view.visibility = View.GONE
    }
}