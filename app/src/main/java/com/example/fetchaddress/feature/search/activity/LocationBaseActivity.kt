package com.example.fetchaddress.feature.search.activity

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.IntentSender.SendIntentException
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.core.app.ActivityCompat
import com.example.fetchaddress.core.base.DaggerBaseActivity
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.ResultCallback
import com.google.android.gms.location.*
import com.google.android.gms.maps.model.LatLng
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.RuntimePermissions

@RuntimePermissions
abstract class LocationBaseActivity : DaggerBaseActivity(), GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private var mGoogleApiClient: GoogleApiClient? = null
    private var mLastLocation: Location? = null
    private var mLocationRequest: LocationRequest? = null
    private val REQUESTLOCATION = 10
    private val STARTGPS = 1000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        checkLocationWithPermissionCheck()
    }

    /*Ending the updates for the location service*/
    override fun onStop() {
        mGoogleApiClient?.disconnect()
        super.onStop()
    }

    override fun onConnected(bundle: Bundle?) {
        settingRequest()
    }

    override fun onConnectionSuspended(i: Int) {
        Toast.makeText(this, "Connection Suspended!", Toast.LENGTH_SHORT).show()
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        Toast.makeText(this, "Connection Failed!", Toast.LENGTH_SHORT).show()
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, 90000)
            } catch (e: SendIntentException) {
                e.printStackTrace()
            }

        } else {
            Log.i("Current Location", "Location services connection failed with code " + connectionResult.errorCode)
        }
    }

    /*Method to get the enable location settings dialog*/
    private fun settingRequest() {
        mLocationRequest = LocationRequest()
        mLocationRequest?.let { mLocationRequest ->
            mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

            val builder = LocationSettingsRequest.Builder()
                    .addLocationRequest(mLocationRequest)

            val result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient,
                    builder.build())

            result.setResultCallback(ResultCallback<LocationSettingsResult> { result ->
                val status = result.status
                val state = result.locationSettingsStates
                when (status.statusCode) {
                    LocationSettingsStatusCodes.SUCCESS ->
                        // All location settings are satisfied. The client can
                        // initialize location requests here.
                        getLocation()
                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED ->
                        // Location settings are not satisfied, but this can be fixed
                        // by showing the user a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(this@LocationBaseActivity, STARTGPS)
                        } catch (e: SendIntentException) {
                            // Ignore the error.
                        }
                    LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                    }
                }
                // Location settings are not satisfied. However, we have no way
                // to fix the settings so we won't show the dialog.
            })
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            STARTGPS -> {
                if (resultCode == Activity.RESULT_OK) {
                    getLocation()
                } else {
                    getNetworkLocation()
                }
            }
        }
    }

    @NeedsPermission(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
    fun checkLocation() {
        enableGoogleClient()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        // NOTE: delegate the permission handling to generated function
        onRequestPermissionsResult(requestCode, grantResults)
    }

    private fun enableGoogleClient() {
        mGoogleApiClient = GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build()
        mGoogleApiClient?.connect()
    }

    private fun getLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION), REQUESTLOCATION)
            return
        } else {
            /*Getting the location after aquiring location service*/
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                    mGoogleApiClient)

            mLastLocation?.let { location ->
                setLatLong(LatLng(location.latitude, location.longitude))
            } ?: run {
                /*if there is no last known location. Which means the device has no data for the loction currently.
               * So we will get the current location.
               * For this we'll implement Location Listener and override onLocationChanged*/
                Log.i("Current Location", "No data for location found")

                if (mGoogleApiClient?.isConnected == false)
                    mGoogleApiClient?.connect()

                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this@LocationBaseActivity)
            }
        }
    }

    /*When Location changes, this method get called. */
    override fun onLocationChanged(location: Location) {
        mLastLocation = location
        setLatLong(LatLng(location.latitude, location.longitude))
    }

    abstract fun setLatLong(latLng: LatLng)
    abstract fun getNetworkLocation()
}
