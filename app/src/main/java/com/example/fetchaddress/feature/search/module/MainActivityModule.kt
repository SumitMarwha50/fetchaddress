package com.example.fetchaddress.feature.search.module

import com.example.fetchaddress.feature.search.fragment.SearchFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class MainActivityModule {

    @ContributesAndroidInjector
    abstract fun contributeSearchFragment(): SearchFragment
}