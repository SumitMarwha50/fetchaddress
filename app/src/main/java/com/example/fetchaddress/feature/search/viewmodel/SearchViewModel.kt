package com.example.fetchaddress.feature.search.viewmodel

import android.os.Handler
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableField
import androidx.databinding.ObservableList
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.example.fetchaddress.BR
import com.example.fetchaddress.R
import com.example.fetchaddress.core.base.BaseViewModel
import com.example.fetchaddress.core.data.ResultState
import com.example.fetchaddress.feature.search.dto.Address
import com.example.fetchaddress.feature.search.dto.FetchAddressResponse
import com.example.fetchaddress.feature.search.repository.SearchRepository
import me.tatarka.bindingcollectionadapter2.ItemBinding
import javax.inject.Inject

class SearchViewModel @Inject constructor(
    private val searchRepository: SearchRepository
): BaseViewModel() {

    var city = ObservableField<String>()
    var query = ObservableField<String>()
    private val handler = Handler()
    val addressList: ObservableList<Address> = ObservableArrayList<Address>()
    var addressItemBinding = ItemBinding.of<Address>(BR.address, R.layout.address_list)
    val mutableAddressList = MutableLiveData<ResultState<FetchAddressResponse>>()
    val liveAddressList: LiveData<ResultState<FetchAddressResponse>> = Transformations.map(mutableAddressList, ::parseResponse)

    private fun parseResponse(response: ResultState<FetchAddressResponse>): ResultState<FetchAddressResponse> {
        when (response) {
            is ResultState.Success -> {
                setRefreshingState(false)
                addressList.clear()
                response.data.addressList?.let {
                    addressList.addAll(response.data.addressList)
                }
            }
            is ResultState.Loading -> {
                setRefreshingState(true)
            }
            is ResultState.Error -> {
                setRefreshingState(false)
                setSnackBarMessage(response.error.errorMessage)
            }
        }
        return response
    }

    fun textChange(newText: String) {
        cancelHandler()
        query.set(newText)
        if(newText.isEmpty()) {
            query.set("")
            addressList.clear()
        } else {
            handler.postDelayed({
                searchRepository.getAddress(mutableAddressList, newText, city.get() ?: "")
            }, 500)
        }
    }

    fun cancelHandler() {
        handler.removeCallbacksAndMessages(null)
    }
}