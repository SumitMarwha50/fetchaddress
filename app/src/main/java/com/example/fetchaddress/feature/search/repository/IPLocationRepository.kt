package com.example.fetchaddress.feature.search.repository

import androidx.lifecycle.MutableLiveData
import com.example.fetchaddress.R
import com.example.fetchaddress.core.utils.Urls
import com.example.fetchaddress.feature.search.dto.IPLocationDto
import com.example.fetchaddress.feature.search.dto.LatLngDto
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class IPLocationRepository @Inject constructor(private val apiService: IPLocationApiService) {

    fun fetchIPLocationData(data: MutableLiveData<IPLocationDto?>) {
        val url = Urls.url_fetch_ip_country
        apiService.fetchIPLocationData(url).enqueue(object : Callback<IPLocationDto> {
            override fun onFailure(call: Call<IPLocationDto>, t: Throwable) {
                data.postValue(null)
            }

            override fun onResponse(call: Call<IPLocationDto>, response: Response<IPLocationDto>) {
                data.postValue(response.body())
            }
        })
    }

    fun fetchLatLngAddress(data: MutableLiveData<LatLngDto?>, lat: String, lng: String) {
        val url = Urls.url_fetch_lat_long_geolocation
        apiService.fetchAddressFromLatLng(url, "fc097e0a0276c6", lat, lng, "json").enqueue(object : Callback<LatLngDto> {
            override fun onFailure(call: Call<LatLngDto>, t: Throwable) {
                data.postValue(null)
            }

            override fun onResponse(call: Call<LatLngDto>, response: Response<LatLngDto>) {
                data.postValue(response.body())
            }
        })

    }
}