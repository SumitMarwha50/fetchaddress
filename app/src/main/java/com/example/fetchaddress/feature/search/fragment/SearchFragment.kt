package com.example.fetchaddress.feature.search.fragment

import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.fetchaddress.R
import com.example.fetchaddress.core.base.DaggerBaseFragment
import com.example.fetchaddress.databinding.FragmentSearchAddressBinding
import com.example.fetchaddress.feature.search.viewmodel.SearchViewModel
import com.example.fetchaddress.utils.snackbar

class SearchFragment: DaggerBaseFragment<SearchViewModel, FragmentSearchAddressBinding>() {

    override fun layoutId(): Int = R.layout.fragment_search_address

    override fun bindVariables() {
        viewBinding.viewModel = viewModel
    }

    override fun initializeViewModel(): SearchViewModel {
        return ViewModelProvider(requireActivity(), viewModelFactory).get(SearchViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setObserver()
        setListener()
    }

    private fun setObserver() {
        viewModel.liveAddressList.observe(viewLifecycleOwner, Observer {
            // dummy to get address
        })
        viewModel.snackBarMessage.observe(viewLifecycleOwner, Observer {
            it.snackbar(viewBinding.root)
        })
    }

    private fun setListener() {
        viewBinding.searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                hideKeyboard()
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                newText?.let {
                    viewModel.textChange(newText)
                }
                return true
            }
        })
    }

    override fun onPause() {
        viewModel.cancelHandler()
        super.onPause()
    }
}