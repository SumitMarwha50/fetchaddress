package com.example.fetchaddress.feature.search.repository

import com.example.fetchaddress.feature.search.dto.IPLocationDto
import com.example.fetchaddress.feature.search.dto.LatLngDto
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

interface IPLocationApiService {
    @GET
    fun fetchIPLocationData(@Url url: String): Call<IPLocationDto>

    @GET
    fun fetchAddressFromLatLng(
        @Url url: String,
        @Query("key") key: String,
        @Query("lat") lat: String,
        @Query("lon") lon: String,
        @Query("format") format: String
    ): Call<LatLngDto>
}
