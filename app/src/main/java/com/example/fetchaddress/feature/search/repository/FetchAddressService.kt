package com.example.fetchaddress.feature.search.repository

import com.example.fetchaddress.core.data.BaseResponse
import com.example.fetchaddress.feature.search.dto.FetchAddressResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

interface FetchAddressService {

    @GET
    fun getAddress(
        @Url url: String,
        @Query("queryString") queryString: String,
        @Query("city") city: String
    ): Call<BaseResponse<FetchAddressResponse>>

}