package com.example.fetchaddress.feature.search.activity

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import com.example.fetchaddress.R
import com.example.fetchaddress.databinding.ActivityMainBinding
import com.example.fetchaddress.feature.search.viewmodel.IPLocationViewModel
import com.example.fetchaddress.feature.search.viewmodel.SearchViewModel
import com.google.android.gms.maps.model.LatLng

class MainActivity: LocationBaseActivity() {

    private lateinit var activityBinding: ActivityMainBinding

    private val ipLocationViewModel: IPLocationViewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(IPLocationViewModel::class.java)
    }

    private val searchViewModel: SearchViewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(SearchViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        navController = Navigation.findNavController(this@MainActivity, R.id.nav_host_fragment)
        setObserver()
    }

    override fun setLatLong(latLng: LatLng) {
        ipLocationViewModel.fetchLatLngAddress("${latLng.latitude}", "${latLng.longitude}")
    }

    override fun getNetworkLocation() {
        ipLocationViewModel.fetchIPLocationData()
    }

    private fun setObserver() {
        ipLocationViewModel.ipLocLiveData.observe(this, Observer {
            it?.city?.let {
                searchViewModel.city.set(it)
            }
        })
        ipLocationViewModel.latLngLiveData.observe(this, Observer {
            it?.address?.let {
                it.county?.let {
                    searchViewModel.city.set(it)
                } ?: it.city?.let {
                    searchViewModel.city.set(it)
                } ?: it.state?.let {
                    searchViewModel.city.set(it)
                }
            } ?: ipLocationViewModel.fetchIPLocationData()
        })
    }
}
