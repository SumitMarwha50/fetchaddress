package com.example.fetchaddress.feature.search.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.example.fetchaddress.core.base.BaseViewModel
import com.example.fetchaddress.feature.search.dto.IPLocationDto
import com.example.fetchaddress.feature.search.dto.LatLngDto
import com.example.fetchaddress.feature.search.repository.IPLocationRepository
import javax.inject.Inject

class IPLocationViewModel @Inject constructor(private val repository: IPLocationRepository) : BaseViewModel() {

    private val ipLocLiveDataInternal = MutableLiveData<IPLocationDto?>()
    val ipLocLiveData = Transformations.map(ipLocLiveDataInternal, ::parseIPLocation)
    private val latLngLocation = MutableLiveData<LatLngDto?>()
    val latLngLiveData = Transformations.map(latLngLocation, ::parseLatLngAddress)

    private fun parseIPLocation(data: IPLocationDto?): IPLocationDto? {
        return data
    }

    private fun parseLatLngAddress(data: LatLngDto?): LatLngDto? {
        return data
    }

    fun fetchIPLocationData() {
        repository.fetchIPLocationData(ipLocLiveDataInternal)
    }

    fun fetchLatLngAddress(lat: String, lng: String) {
        repository.fetchLatLngAddress(latLngLocation, lat, lng)
    }
}