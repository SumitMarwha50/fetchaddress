package com.example.fetchaddress.feature.search.dto

import com.google.gson.annotations.SerializedName

data class LatLngDto (
    @SerializedName("address")
    val address: AddressLatLng? = null
)

data class AddressLatLng (
    @SerializedName("city")
    val city: String? = null,
    @SerializedName("county")
    val county: String? = null,
    @SerializedName("state")
    val state: String? = null,
    @SerializedName("country")
    val country: String? = null
)