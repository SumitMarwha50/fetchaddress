package com.example.fetchaddress.core.network

import androidx.lifecycle.MutableLiveData
import com.example.fetchaddress.R
import com.example.fetchaddress.core.data.BaseResponse
import com.example.fetchaddress.core.data.ErrorEntity
import com.example.fetchaddress.core.data.ResultState
import com.example.fetchaddress.core.network.exception.NoInternetException
import com.google.gson.JsonSyntaxException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException
import java.net.ConnectException
import java.net.UnknownHostException

class ResponseListenerHandling<T>(
    private val responseMutableLiveData: MutableLiveData<ResultState<T>>
): Callback<BaseResponse<T>> {
    override fun onResponse(call: Call<BaseResponse<T>>, response: Response<BaseResponse<T>>) {
        if (response.isSuccessful) {
            response.body()?.let {
                it.data?.let { data ->
                    responseMutableLiveData.postValue(ResultState.Success(data))
                } ?: responseMutableLiveData.postValue(
                    ResultState.Error(
                        ErrorEntity.Error(
                            response.code().toString(),
                            response.message() ?: R.string.no_data_found
                        )
                    )
                )
            } ?: responseMutableLiveData.postValue(
                ResultState.Error(
                    ErrorEntity.Error(
                        response.code().toString(),
                        response.message() ?: R.string.something_went_wrong_please_try
                    )
                )
            )
        } else {
            responseMutableLiveData.postValue(
                ResultState.Error(
                    ErrorEntity.Error(
                        response.code().toString(),
                        response.errorBody().toString()
                    )
                )
            )
        }
    }

    override fun onFailure(call: Call<BaseResponse<T>>, t: Throwable) {
        responseMutableLiveData.postValue(ResultState.Error(handleErrorReturn(t).error))
    }
}

internal fun handleErrorReturn(throwable: Throwable): ResultState.Error<ErrorEntity.Error> {
    return when (throwable) {
        is HttpException -> {
            when (throwable.code()) {
                NetworkConfig.RESPONSE_CODE_NOT_FOUND -> {
                    ResultState.Error(
                        ErrorEntity.Error(
                            NetworkConfig.RESPONSE_CODE_NOT_FOUND.toString(),
                            R.string.something_went_wrong_please_try
                        )
                    )
                }
                NetworkConfig.RESPONSE_CODE_DEACTIVATED -> {
                    ResultState.Error(
                        ErrorEntity.Error(
                            NetworkConfig.RESPONSE_CODE_DEACTIVATED.toString(),
                            R.string.something_went_wrong_please_try
                        )
                    )
                }
                NetworkConfig.RESPONSE_CODE_REQUEST_TIMEOUT -> {
                    ResultState.Error(
                        ErrorEntity.Error(
                            NetworkConfig.RESPONSE_CODE_REQUEST_TIMEOUT.toString(),
                            R.string.we_are_unable_to_we
                        )
                    )
                }
                NetworkConfig.RESPONSE_CODE_REQUEST_TIMEOUT_1 -> {
                    ResultState.Error(
                        ErrorEntity.Error(
                            NetworkConfig.RESPONSE_CODE_REQUEST_TIMEOUT.toString(),
                            R.string.we_are_unable_to_we
                        )
                    )
                }
                NetworkConfig.RESPONSE_CODE_LARGE_FILE -> {
                    ResultState.Error(
                        ErrorEntity.Error(
                            NetworkConfig.RESPONSE_CODE_REQUEST_TIMEOUT.toString(),
                            R.string.we_are_unable_to_we
                        )
                    )
                }
                else -> {
                    ResultState.Error(
                        ErrorEntity.Error(
                            NetworkConfig.RESPONSE_CODE_REQUEST_TIMEOUT.toString(),
                            R.string.we_are_unable_to_we
                        )
                    )
                }
            }
        }
        is NoInternetException -> {
            ResultState.Error(
                ErrorEntity.Error(
                    NetworkConfig.RESPONSE_CODE_NETWORK_ERROR.toString(),
                    R.string.you_are_not_connected_to
                )
            )
        }
        is UnknownHostException, is IOException, is ConnectException -> {
            ResultState.Error(
                ErrorEntity.Error(
                    NetworkConfig.RESPONSE_CODE_NETWORK_ERROR.toString(),
                    R.string.something_went_wrong_please_try
                )
            )
        }
        is JsonSyntaxException -> {
            ResultState.Error(
                ErrorEntity.Error(
                    NetworkConfig.RESPONSE_CODE_SERVER_ERROR.toString(),
                    R.string.something_went_wrong_please_try
                )
            )
        }
        else -> {
            ResultState.Error(
                ErrorEntity.Error(
                    NetworkConfig.RESPONSE_CODE_UNEXPECTED.toString(),
                    R.string.something_went_wrong_please_try
                )
            )
        }
    }
}