package com.example.fetchaddress.core.network.interceptor

import com.example.fetchaddress.core.network.exception.NoInternetException
import com.example.fetchaddress.core.network.utils.NetworkManager
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NetworkConnectionInterceptor @Inject constructor(private val networkManager: NetworkManager) :
    Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        if (!isInternetAvailable()) {
            throw NoInternetException()
        }
        return chain.proceed(chain.request())
    }

    private fun isInternetAvailable(): Boolean {
        return networkManager.isConnectedToInternet
    }
}