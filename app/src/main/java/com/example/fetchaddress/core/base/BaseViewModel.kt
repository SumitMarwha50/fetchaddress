package com.example.fetchaddress.core.base

import android.os.Bundle
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import androidx.navigation.fragment.FragmentNavigator
import com.example.fetchaddress.core.network.utils.SingleLiveEvent

open class BaseViewModel: ViewModel() {

    val snackBarMessage = SingleLiveEvent<Any>()
    val refreshState = ObservableField(false)
    val navigationWithBundle = SingleLiveEvent<Triple<Int, Bundle?, FragmentNavigator.Extras?>>()

    fun setSnackBarMessage(message: Any) {
        snackBarMessage.postValue(message)
    }

    fun setRefreshingState(refreshing: Boolean) {
        refreshState.set(refreshing)
    }

    fun setNavigationWithBundle(resourceId: Int, bundle: Bundle? = null, extras: FragmentNavigator.Extras? = null) {
        navigationWithBundle.value = Triple(resourceId, bundle, extras)
    }
}