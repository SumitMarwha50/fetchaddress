package com.example.fetchaddress.core.network

object NetworkConfig {

    const val RESPONSE_CODE_NOT_FOUND = 404
    const val RESPONSE_CODE_SERVER_ERROR = 500

    const val RESPONSE_CODE_DEACTIVATED = 1
    const val RESPONSE_CODE_REQUEST_TIMEOUT = 2
    const val RESPONSE_CODE_REQUEST_TIMEOUT_1 = 3
    const val RESPONSE_CODE_LARGE_FILE = 4
    const val RESPONSE_CODE_UNEXPECTED = 5
    const val RESPONSE_CODE_NETWORK_ERROR = 6
}