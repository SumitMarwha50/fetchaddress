package com.example.fetchaddress.core.data

import com.google.gson.annotations.SerializedName

data class BaseResponse<T> constructor(
    @SerializedName("requestId") val requestId: String,
    @SerializedName("data") val data: T? = null
)