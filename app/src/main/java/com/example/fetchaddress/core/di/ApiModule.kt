package com.example.fetchaddress.core.di

import com.example.fetchaddress.feature.search.repository.FetchAddressService
import com.example.fetchaddress.feature.search.repository.IPLocationApiService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class APIModule {

    @Provides
    fun providesNewsAPI(retrofit: Retrofit): FetchAddressService =
        retrofit.create(FetchAddressService::class.java)

    @Provides
    fun providesIPLocationApiService(retrofit: Retrofit): IPLocationApiService =
        retrofit.create(IPLocationApiService::class.java)

}