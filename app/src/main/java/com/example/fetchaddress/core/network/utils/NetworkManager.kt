package com.example.fetchaddress.core.network.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.NetworkInfo
import android.os.Build
import javax.inject.Inject

class NetworkManager @Inject constructor(private val applicationContext: Context) {
    val isConnectedToInternet: Boolean
        get() = with(applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager) {
            isConnectedToInternet()
        }
}

fun ConnectivityManager.isConnectedToInternet(): Boolean =
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        isConnected(
            getNetworkCapabilities(
                activeNetwork
            )
        )
    } else {
        isConnected(activeNetworkInfo)
    }

@Suppress("DEPRECATION")
fun isConnected(network: NetworkInfo?): Boolean {
    return when (network) {
        null -> false
        else -> with(network) { isConnected && (type == ConnectivityManager.TYPE_WIFI || type == ConnectivityManager.TYPE_MOBILE) }
    }
}

fun isConnected(networkCapabilities: NetworkCapabilities?): Boolean {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        when (networkCapabilities) {
            null -> false
            else -> with(networkCapabilities) {
                hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) || hasTransport(
                    NetworkCapabilities.TRANSPORT_WIFI
                )
            }
        }
    } else {
        false
    }

}