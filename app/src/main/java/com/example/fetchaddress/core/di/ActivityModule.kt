package com.example.fetchaddress.core.di

import com.example.fetchaddress.feature.search.activity.MainActivity
import com.example.fetchaddress.feature.search.module.MainActivityModule
import com.example.fetchaddress.core.base.DaggerBaseActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class ActivityModule {

    @ContributesAndroidInjector
    abstract fun contributeDaggerBaseActivity(): DaggerBaseActivity

    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    abstract fun contributeMainActivity(): MainActivity
}