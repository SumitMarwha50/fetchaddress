package com.example.fetchaddress.core.utils

object Urls {

    const val addressList = "/compassLocation/rest/address/autocomplete"
    const val url_fetch_ip_country = "https://freegeoip.app/json/"
    const val url_fetch_lat_long_geolocation = "https://us1.locationiq.com/v1/reverse.php/"

}