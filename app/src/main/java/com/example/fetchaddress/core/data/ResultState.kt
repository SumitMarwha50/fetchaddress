package com.example.fetchaddress.core.data

sealed class ResultState<T> {

    /**
     * A state of Loading which shows that there is still an update to come.
     */
    data class Loading<T>(val showLoading: Boolean) : ResultState<T>()

    /**
     * A state that shows the [data] is the last known update.
     */
    data class Success<T>(val data: T) : ResultState<T>()

    /**
     * A state to show a [error] is thrown.
     */
    data class Error<T>(val error: ErrorEntity.Error) : ResultState<T>()
}