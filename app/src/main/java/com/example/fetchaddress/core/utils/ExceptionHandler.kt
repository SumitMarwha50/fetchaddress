package com.example.fetchaddress.core.utils

import android.annotation.SuppressLint
import android.util.Log
import androidx.databinding.library.BuildConfig

object ExceptionHandler {
    private const val TAG = "AccessMoney :: ExceptionHandler"

    @SuppressLint("LongLogTag")
    @JvmStatic
    fun handle(e: Exception?) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, Log.getStackTraceString(e))
        }
    }
}