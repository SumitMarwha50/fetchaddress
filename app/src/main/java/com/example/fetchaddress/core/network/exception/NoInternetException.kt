package com.example.fetchaddress.core.network.exception

import java.io.IOException

class NoInternetException : IOException()