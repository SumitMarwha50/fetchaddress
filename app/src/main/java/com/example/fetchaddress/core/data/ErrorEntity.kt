package com.example.fetchaddress.core.data

sealed class ErrorEntity {

    data class Error(val errorCode: String, val errorMessage: Any) : ErrorEntity()

}