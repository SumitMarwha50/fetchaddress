package com.example.fetchaddress.core.di

import android.content.Context
import android.util.Log
import com.example.fetchaddress.BuildConfig
import com.example.fetchaddress.core.utils.ExceptionHandler
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideGsonConverterFactory(): Converter.Factory {
        return GsonConverterFactory.create()
    }

    @Singleton
    @Provides
    fun provideRetrofit(
        converterFactory: Converter.Factory,
        httpLoggingInterceptor: HttpLoggingInterceptor,
        context: Context
    ): Retrofit {
        val httpClient = OkHttpClient.Builder()
        if (BuildConfig.DEBUG)
            httpClient.addInterceptor(httpLoggingInterceptor)
        return Retrofit.Builder()
            .addConverterFactory(converterFactory)
            .baseUrl(BuildConfig.DOMAIN_SEARCH)
            .client(httpClient.build())
            .build()
    }

    @Singleton
    @Provides
    fun createLoggingInterceptor(): HttpLoggingInterceptor {
        val logging: HttpLoggingInterceptor
        logging = HttpLoggingInterceptor { message ->
            try {
                Log.d("Retrofit :: ", message)
            } catch (ignore: Exception) {
                ExceptionHandler.handle(ignore)
            }
        }
        logging.level = HttpLoggingInterceptor.Level.BODY
        return logging
    }
}