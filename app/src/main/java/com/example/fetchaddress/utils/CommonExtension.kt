package com.example.fetchaddress.utils

import android.content.res.Resources
import android.view.View
import com.example.fetchaddress.R
import com.google.android.material.snackbar.Snackbar

fun Any?.snackbar(view: View, duration: Int = Snackbar.LENGTH_LONG): Snackbar {
    var message = view.context.getString(R.string.something_went_wrong_please_try)
    this?.let { any ->
        when (any) {
            is String -> message = any
            is Int -> {
                try {
                    view.context.resources.getResourceName(any)?.let {
                        message = view.context.getString(any)
                    }
                } catch (e: Resources.NotFoundException) {
                }
            }
            else -> message = view.context.getString(R.string.something_went_wrong_please_try)
        }
    }
    return Snackbar.make(view, message, duration).apply { show() }
}