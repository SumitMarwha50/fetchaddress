package com.example.fetchaddress

import androidx.multidex.MultiDex
import com.example.fetchaddress.core.di.AppComponent
import com.example.fetchaddress.core.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class FetchAddressApplication: DaggerApplication() {

    override fun onCreate() {
        super.onCreate()
        sInstance = this
        MultiDex.install(this)
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        appComponent = DaggerAppComponent.builder().application(this).build()
        return appComponent
    }

    companion object {
        private lateinit var sInstance: FetchAddressApplication
        private lateinit var appComponent: AppComponent

        @Synchronized
        fun getInstance(): FetchAddressApplication {
            return sInstance
        }

        @Synchronized
        fun getCoreComponent(): AppComponent {
            return appComponent
        }
    }
}